package be.kdg.java.tryitout.shop.tr01_interfaces.products;

import java.util.Objects;

/**
 * @author Christian Cambier
 * @version 1.0     09/2022
 */
public abstract class Product {
    /*
      * Instance data member(s)
     */
    private String code;
    private String description;
    private double price;
    
    /*
      Constructor(s)
     */
    protected Product(String code, String description, double price) {
        setCode(code);
        setDescription(description);
        setPrice(price);
    }  // ctor
    
    /*
      Getter(s)/Setter(s)
     */
    public String getCode() {
        return code;
    }
    
    public final void setCode(String code) {
        this.code = code;
    }
    
    public String getDescription() {
        return description;
    }
    
    public final void setDescription(String description) {
        this.description = description;
    }
    
    public double getPrice() {
        return price;
    }
    
    public final void setPrice(double price) {
        this.price = price;
    }
    
    /*
      Method(s)
    */
    @Override
    public String toString() {
        return String.format("code:%-8s  description:%-30s  price(Eur):%-6.2f  ", getCode(), getDescription(),
                getPrice());
    }  // toString()
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        
        Product product = (Product) o;
        
        if (Double.compare(product.price, price) != 0) {
            return false;
        }
        if (!Objects.equals(code, product.code)) {
            return false;
        }
        return Objects.equals(description, product.description);
    }
    
    @Override
    public int hashCode() {
        int result;
        long temp;
        result = code != null ? code.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
    
    public abstract double getVat();
}  // class